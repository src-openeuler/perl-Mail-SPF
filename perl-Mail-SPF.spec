Name:           perl-Mail-SPF
Version:        3.20240923
Release:        2
Summary:        Sender Policy Framework's object-oriented implementation
License:        BSD-3-Clause
URL:            https://metacpan.org/release/Mail-SPF
Source0:        https://cpan.metacpan.org/modules/by-module/Mail/Mail-SPF-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  coreutils perl-interpreter perl-generators perl(Module::Build) perl(version)
BuildRequires:  perl(base) perl(constant) perl(Net::DNS) >= 0.62 perl(Net::DNS::Resolver)
BuildRequires:  perl(NetAddr::IP) >= 4 perl(overload) perl(strict) perl(Sys::Hostname) perl(URI) >= 1.13
BuildRequires:  perl(URI::Escape) perl(utf8) perl(warnings) perl(blib)
BuildRequires:  perl(Net::DNS::Resolver::Programmable) >= 0.003 perl(Net::DNS::RR) perl(Test::More)
BuildRequires:  perl(URI::Escape) perl(utf8) perl(warnings) perl(blib) perl(Test::Pod) >= 1.00
BuildRequires:  perl-Error

Requires:       perl(Net::DNS) >= 0.62 perl(URI) >= 1.13
Requires(post):   chkconfig
Requires(postun): chkconfig

%description
Mail::SPF is the Sender Policy Framework's object-oriented implementation in Perl.

%package_help

%prep
%autosetup -n Mail-SPF-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}

%install
%{make_install}
%{_fixperms} %{buildroot}/*
mv -f %{buildroot}%{_bindir}/spfquery %{buildroot}%{_bindir}/spfquery.%{name}
mv -f %{buildroot}%{_bindir}/spfd %{buildroot}%{_bindir}/spfd.%{name}
mv -f %{buildroot}%{_mandir}/man1/spfquery.1 %{buildroot}%{_mandir}/man1/spfquery-%{name}.1
touch %{buildroot}%{_bindir}/spfquery %{buildroot}%{_bindir}/spfd %{buildroot}%{_mandir}/man1/spfquery.1.gz

%check
make test

%post
/usr/sbin/update-alternatives --install %{_bindir}/spfquery spf %{_bindir}/spfquery.%{name} 10 \
--slave %{_mandir}/man1/spfquery.1.gz spfquery-man-page %{_mandir}/man1/spfquery-%{name}.1.gz \
--slave %{_bindir}/spfd spf-daemon %{_bindir}/spfd.%{name}

%postun
if [ $1 -eq 0 ] ; then
/usr/sbin/update-alternatives --remove spf %{_bindir}/spfquery.%{name}
fi

%files
%license LICENSE
%{perl_vendorlib}/*
%{_bindir}/spfquery.%{name}
%{_bindir}/spfd.%{name}
%ghost %{_bindir}/spfquery
%ghost %{_bindir}/spfd

%files help
%doc Changes README TODO
%{_mandir}/man1/spf*
%{_mandir}/man3/Mail::SPF*
%ghost %{_mandir}/man1/spfquery.1.gz

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 3.20240923-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Sep 24 2024 wangkai <13474090681@163.com> - 3.20240923-1
- Update to 3.20240923

* Mon May 09 2022 houyingchao <houyingchao@h-partners.com> - 2.9.0-20
- License compliance rectification

* Sat Nov 30 2019 Shuaishuai Song <songshuaishuai2@huawei.com> - 2.9.0-19
- package init
